# Shibboleth SP

## Goal
Shibboleth SP using Docker that works within DFN federation.

## What have I done?!
I use `unicon/shibboleth-sp` as base image and so far have just changed few lines in `shibboleth2.xml`. Basically I've just added a `<MetadataProvider>` and a specific IdP to use within `<SSO>`, following the steps here:
https://wiki.shibboleth.net/confluence/display/SP3/GettingStarted

The Metadata is located at http://www.aai.dfn.de/fileadmin/metadata/dfn-aai-test-metadata.xml and the IdPs entityID I chose is `https://testidp.aai.dfn.de/idp/shibboleth`

## Steps to reproduce
You'll need Docker on your machine. I use Windows, so there are some `.bat` files around. They have only few lines, Linux guys/girls should not have any problems adapting ;)

- Build the Docker image (`build-docker.bat`), it will just integrate 
    the configuration from `/shibboleth` into the docker image from `unicon/shibboleth-sp`
- Create a Docker network `docker network create shibb.net`
- Start the built image using `test-docker.bat`, it will also start the debug webserver
- Navigate to http://localhost/secure in the Browser, you'll get redirected to the IdP

## Random Links
https://wiki.shibboleth.net/confluence/display/SP3/GettingStarted
https://wiki.shibboleth.net/confluence/display/SP3/MetadataProvider
https://download.aai.dfn.de/praesentationen/zki_ak_vd_2019_01/einfuehrung_idp_michels.pdf
https://wiki.shibboleth.net/confluence/display/SP3/UpgradingFromV2
https://doku.tid.dfn.de/de:shibsp
https://doku.tid.dfn.de/de:urnreg:start
https://www.unicon.net/insights/blogs/unicon-iam-docker-images