docker ps
docker stop shibbsp
:: Start the actual Shibboleth SP
start docker run --rm --network shibb.net -p 80:80 -p 443:443 --name shibbsp shibbsp
timeout 20
:: Open Browser
start "" "http://localhost/secure"