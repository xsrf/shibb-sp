FROM unicon/shibboleth-sp
# Download DFN cert required for metadata validation
ADD https://www.aai.dfn.de/fileadmin/metadata/dfn-aai.g2.pem /etc/shibboleth/dfn-aai.g2.pem
# Download Metadata locally (for debugging only)
ADD https://www.aai.dfn.de/fileadmin/metadata/dfn-aai-test-metadata.xml /etc/shibboleth/metadata/dfn-aai-test-metadata.xml
# Integrate config
ADD shibboleth/ /etc/shibboleth/
# Make sure shibd can read all files
RUN chown -R shibd:shibd /etc/shibboleth/
